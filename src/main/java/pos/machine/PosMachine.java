package pos.machine;

import java.util.*;

public class PosMachine {
    public String printReceipt(List<String> barcodes) {
        Map<String, Item> itemMap = getItemMap();
        Map<Item, Integer> itemCountMap = getItemCountMap(barcodes, itemMap);
        String itemsInfo = genItemsInfo(barcodes, itemMap, itemCountMap);
        String receiptHead = genReceipted();
        String totalInfo = genReceiptTotal(itemCountMap);
        return receiptHead + itemsInfo + totalInfo;
    }

    public Map<String, Item> getItemMap(){
        Map<String, Item> map = new HashMap<>();
        List<Item> list = ItemsLoader.loadAllItems();
        for (Item good : list){
            map.put(good.getBarcode(), good);
        }
        return map;
    }

    public boolean checkBarcode(String barcode, Map<String, Item> itemMap){
        return itemMap.containsKey(barcode);
    }

    public Item getItem(String barcode, Map<String, Item> itemMap){
        if (!checkBarcode(barcode, itemMap))return null;
        return itemMap.get(barcode);
    }

    public Map<Item, Integer> getItemCountMap(List<String> barcodes, Map<String, Item> itemMap){
        Map<Item, Integer> itemCountMap = new HashMap<>();
        for (String barcode : barcodes){
            Item item = getItem(barcode, itemMap);
            if (!itemCountMap.containsKey(item)){
                itemCountMap.put(item, 1);
            }else{
                itemCountMap.replace(item, itemCountMap.get(item) + 1);
            }
        }
        return itemCountMap;
    }

    public String getItemInfo(Item item, int num){
        int subtotal = item.getPrice() * num;
        return "Name: "+ item.getName() + ", Quantity: " + num + ", Unit price: " + item.getPrice()
                + " (yuan), Subtotal: " + subtotal + " (yuan)\n";
    }

    public String genItemsInfo(List<String> barcodes, Map<String, Item> itemMap, Map<Item, Integer> itemCountMap){
        Set<String> set = new LinkedHashSet<>(barcodes);
        StringBuilder builder = new StringBuilder();
        for (String barcode : set){
            Item item = itemMap.get(barcode);
            if (item != null){
                String itemInfo = getItemInfo(item, itemCountMap.get(item));
                builder.append(itemInfo);
            }
        }
        builder.append("----------------------\n");
        return builder.toString();
    }

    public String genReceipted(){
        return "***<store earning no money>Receipt***\n";
    }

    public String genReceiptTotal(Map<Item, Integer> itemCountMap){
        int total = 0;
        for (Map.Entry<Item, Integer> entry: itemCountMap.entrySet()){
            total += entry.getKey().getPrice() * entry.getValue();
        }
        return "Total: " + total +" (yuan)\n" + "**********************";
    }
}

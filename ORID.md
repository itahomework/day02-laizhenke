# Daily Report(2023/07/11)

- O：学习了需求分析，task的拆分以及拆分的三个要点（复杂变简单，独立，可执行），Git的一些简单操作，函数以及变量的命名，ContextMap的制作。
- R：收获颇丰
- I：老师一直强调需求分析的重要性，让我想起了之前写毕设因为需求分析没做好而带来的麻烦，之前的开发Git的提交也没有分步骤，经常是敲到哪里就提交到哪里，提交日志也写的很马虎，这些coding的坏习惯需要改掉。
- D：之前Git的提交日志写的很随便，没有什么章法可言，通过今天的学习，在以后的开发中药尽可能写规范Git的提交日志，还有需求分析一定要做好，这样才能避免发生代码的推倒重来。

​			